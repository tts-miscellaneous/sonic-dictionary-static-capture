(function($) {
  /**
   * The recommended way for producing HTML markup through JavaScript is to write
   * theming functions. These are similiar to the theming functions that you might
   * know from 'phptemplate' (the default PHP templating engine used by most
   * Drupal themes including Omega). JavaScript theme functions accept arguments
   * and can be overriden by sub-themes.
   *
   * In most cases, there is no good reason to NOT wrap your markup producing
   * JavaScript in a theme function.
   */
  //    Drupal.theme.prototype.nrotcExampleButton = function (path, title) {
  //      // Create an anchor element with jQuery.
  //      return $('<a href="' + path + '" title="' + title + '">' + title + '</a>');
  //    };
  /**
   * Behaviors are Drupal's way of applying JavaScript to a page. The advantage
   * of behaviors over simIn short, the advantage of Behaviors over a simple
   * document.ready() lies in how it interacts with content loaded through Ajax.
   * Opposed to the 'document.ready()' event which is only fired once when the
   * page is initially loaded, behaviors get re-executed whenever something is
   * added to the page through Ajax.
   *
   * You can attach as many behaviors as you wish. In fact, instead of overloading
   * a single behavior with multiple, completely unrelated tasks you should create
   * a separate behavior for every separate task.
   *
   * In most cases, there is no good reason to NOT wrap your JavaScript code in a
   * behavior.
   *
   * @param context
   *   The context for which the behavior is being executed. This is either the
   *   full page or a piece of HTML that was just added through Ajax.
   * @param settings
   *   An array of settings (added through drupal_add_js()). Instead of accessing
   *   Drupal.settings directly you should use this because of potential
   *   modifications made by the Ajax callback that also produced 'context'.
   */
  Drupal.behaviors.ttsExampleBehavior = {
    attach: function(context, settings) {
      // By using the 'context' variable we make sure that our code only runs on
      // the relevant HTML. Furthermore, by using jQuery.once() we make sure that
      // we don't run the same piece of code for an HTML snippet that we already
      // processed previously. By using .once('foo') all processed elements will
      // get tagged with a 'foo-processed' class, causing all future invocations
      // of this behavior to ignore them.
      //  $('.some-selector', context).once('foo', function() {
      //    // Now, we are invoking the previously declared theme function using two
      //    // settings as arguments.
      //    var $anchor = Drupal.theme('ttsExampleButton', settings.myExampleLinkPath, settings.myExampleLinkTitle);

      //    // The anchor is then appended to the current element.
      //    $anchor.appendTo(this);
      //  });
      // $(".pager li.previous").filter(function() {
      //   return $.trim($(this).html()) == '';
      // }).remove()
    }
  };

  // remove mobile menu from DOM when not needed
  Drupal.behaviors.hideMmenuModule = {
    attach: function (context, settings) {
      var mobmenu = $('#mm-0').detach();
      if ($(window).width() < 769) {
        mobmenu.appendTo('#mmenu_left');
      }
      $(window).resize(function () {
      console.log($(window).width());
        if ($(window).width() > 769) {
          $('#mm-0').detach();
        }
        else if ($(window).width() < 769) {
          mobmenu.appendTo('#mmenu_left');
        }
      });
    }
  };

  // makes div clickable
  Drupal.behaviors.ttsDivlink = {
    attach: function(context, settings) {
      $('.clickableDiv', context).click(function() {
        window.location = $(this).find('a').attr('href');
        return false;
      });
    }
  };

  // Responsive menu sub-item open/close toggle
  Drupal.behaviors.ttsMenuToggle = {
    attach: function(context, settings) {

      var $expand = $("ul.mmenu-mm-list-level-1 li");
      $expand.click(function(e) {
        var classname = $(this).attr('class');
        $(this).siblings().removeClass('mm-opened');
        $('.' + classname).addClass("mm-opened");
      });

    }
  };

  // FAQ toggler
  // Use with <div class="toggler" style="cursor: pointer;"><div>Show</div></div>
  Drupal.behaviors.faqTogglerModule = {
    attach: function (context, settings) {
      $(".toggler").css("cursor", "pointer");
      $(".toggler").click(function(){
        $(this).toggleClass("opentoggle");
        $(this).children("div").text($(this).text() == 'Show' ? 'Hide' : 'Show');
        $(this).prev().slideToggle(300);
        return false;
      }).prev().hide();
    }
  };

  // force youtube and vimeo shadowbox videos served over https
  Drupal.behaviors.secureVideoModule = {
    attach: function (context, settings) {
      $('a[href^="http://www.youtube.com"], a[href^="http://player.vimeo.com"]').each(function(){
        $(this).attr('href', $(this).attr('href').replace("http://","https://"))
      });
    }
  };

  // responsive tables
  Drupal.behaviors.responsivetablesModule = {
    attach: function (context, settings) {
      (function () {
        var headertext = [];
        var headers = document.querySelectorAll(".l-content thead");
        var tablebody = document.querySelectorAll(".l-content tbody");
        for(var i = 0; i < headers.length; i++) { headertext[i]=[]; for (var j = 0, headrow; headrow = headers[i].rows[0].cells[j]; j++) { var current = headrow; headertext[i].push(current.textContent.replace(/\r?\n|\r/,"")); } } if (headers.length > 0) {
          for (var h = 0, tbody; tbody = tablebody[h]; h++) {
            for (var i = 0, row; row = tbody.rows[i]; i++) {
              for (var j = 0, col; col = row.cells[j]; j++) {
                col.setAttribute("data-th", headertext[h][j]);
              }
            }
          }
        }
      } ());
    }
  };

})(jQuery);
