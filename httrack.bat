"c:\Program Files\WinHTTrack\httrack.exe" --mirror https://sonicdictionary.duke.edu  ^
  -* ^
  +sonicdictionary.duke.edu* ^
  -*/Shibboleth.sso* ^
  -*/node/* ^
  -%%P ^
  -n ^
  -r20 ^
  -C2 ^
  -%%F " " ^
  --disable-security-limits ^
  --advanced-progressinfo ^
  --can-go-up-and-down ^
  --display ^
  --keep-alive ^
  --mirror ^
  --robots=0 ^
  -%%c100 ^
  -c100 ^
  --max-rate=0
